package com.dimaidt.workflow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author Administrator
 */
@SpringBootApplication
public class DimaidtWorkflowApplication {

    public static void main(String[] args) {
        SpringApplication.run(DimaidtWorkflowApplication.class, args);
    }

}
